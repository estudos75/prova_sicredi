package steps;


import io.cucumber.java.it.Quando;
import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Então;
import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.Select;
import runner.RunCucumberTest;

public class AddCostumerStep extends RunCucumberTest {


    @Dado("^que estou no site  https://www\\.grocerycrud\\.com/v(\\d+)\\.x/demo/bootstrap_theme$")
    public void que_estou_no_site_https_www_grocerycrud_com_v_x_demo_bootstrap_theme(int arg1) throws Throwable {
        // Write code here that turns the phrase above into concrete actions

        driver.get("https://www.grocerycrud.com/v1.x/demo/bootstrap_theme");
    }

    @E("^seleciono para mudar o valor da combo Select version para “Bootstrap V(\\d+) Theme”$")
    public void seleciono_para_mudar_o_valor_da_combo_Select_version_para_Bootstrap_V_Theme(int arg1) throws Throwable {
        WebElement comboboxElement = driver.findElement(By.id("switch-version-select"));
        Select combobox = new Select(comboboxElement);
        combobox.selectByIndex(1);
    }

    @E("^clico no botão de Add Customer$")
    public void clico_no_botão_de_Add_Customer() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        driver.findElement(By.xpath("//*[@id=\"gcrud-search-form\"]/div[1]/div[1]/a")).click();
    }

    @E("^preencho os campos do formulário$")
    public void preencho_os_campos_do_formulário() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        driver.findElement(By.id("field-customerName")).sendKeys("Teste Sicredi");
        driver.findElement(By.id("field-contactLastName")).sendKeys("Teste");
        driver.findElement(By.id("field-contactFirstName")).sendKeys("Vanessa Borges");
        driver.findElement(By.id("field-phone")).sendKeys("51 9999-9999");
        driver.findElement(By.id("field-addressLine1")).sendKeys("Av Assis Brasil, 3970");
        driver.findElement(By.id("field-addressLine2")).sendKeys("Torre D");
        driver.findElement(By.id("field-city")).sendKeys("Porto Alegre");
        driver.findElement(By.id("field-state")).sendKeys("RS");
        driver.findElement(By.id("field-postalCode")).sendKeys("91000-000");
        driver.findElement(By.id("field-country")).sendKeys("Brasil");

        WebElement eleChosen = driver.findElement(By.id("field_salesRepEmployeeNumber_chosen"));
        eleChosen.click();

        WebElement eleInput = eleChosen.findElement(By.xpath(".//input"));
        eleInput.sendKeys("Fixter");
        eleInput.sendKeys(Keys.TAB);

        driver.findElement(By.id("field-creditLimit")).sendKeys("200");
    }

    @Quando("^clico no botão Save$")
    public void clico_no_botão_Save() throws Throwable {
        driver.findElement(By.id("form-button-save")).click();

    }

    @Então("^vizualizo uma mensagem de sucesso$")
    public void vizualizo_uma_mensagem_de_sucesso() throws Throwable {
        WebElement webElement = driver.findElement(By.xpath("//div[@class='form-group gcrud-form-group']"));
        String script = "return window.getComputedStyle(arguments[0],':after').length";
        JavascriptExecutor js = (JavascriptExecutor) driver;
        Long content = (Long) js.executeScript(script, webElement);
        System.out.println(content);
        Assert.assertEquals("Não foi encontrado um resultado de sucesso ao tentar cadastrar o usuário",true, content > 0 );

    }


}
